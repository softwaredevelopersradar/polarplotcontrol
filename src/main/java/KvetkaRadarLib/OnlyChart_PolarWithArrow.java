package KvetkaRadarLib;

import KvetkaRadarLib.Chart.ArrowCanvas;
import KvetkaRadarLib.Chart.PolarPlotSeries;
import KvetkaRadarLib.Chart.XYPanePolar;
import eu.hansolo.fx.charts.PolarChart;
import eu.hansolo.fx.charts.PolarTickStep;
import eu.hansolo.fx.charts.data.XYChartItem;
import eu.hansolo.fx.charts.font.Fonts;
import eu.hansolo.fx.charts.tools.Helper;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Parent.*;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OnlyChart_PolarWithArrow extends PolarChart<XYChartItem> {

    private ArrowCanvas canvas;
    private final DoubleProperty degreesProperty = new SimpleDoubleProperty(1.0);
    private final DoubleProperty valueProperty = new SimpleDoubleProperty(1.0);
    public DoubleProperty getDegreesProperty()
    {
        return degreesProperty;
    }
    public DoubleProperty getValueProperty()
    {
        return valueProperty;
    }




    public OnlyChart_PolarWithArrow() {
        super(new XYPanePolar<>(new PolarPlotSeries()));

        this.getStylesheets().add("/cssFiles/Theme.css");
        canvas = new ArrowCanvas();
        canvas.widthProperty().bind(this.widthProperty());
        canvas.heightProperty().bind(this.heightProperty());

        ChangeListener<Number> sizeListener = new ChangeListener<Number>(){

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                UpdateArrow(canvas.getAngle());
                ReInitTextColor();
            }
        };

        this.widthProperty().addListener(sizeListener);
        this.heightProperty().addListener(sizeListener);
        this.getXYPane().widthProperty().addListener(sizeListener);
        this.getXYPane().heightProperty().addListener(sizeListener);

        this.getChildren().add(canvas);
    }


    public ArrowCanvas getArrowCanvas()
    {
        return canvas;
    }


    public <T> void UpdateChart(List<T> data)
    {
        ((PolarPlotSeries)(this.getXYPane().getListOfSeries().get(0))).setData(data);

        this.refresh();

        ReInitTextColor();
    }

    public void UpdateArrow(double angle)
    {
        canvas.UpdateArrow(angle);
        canvas.isResizable();

        this.refresh();

        //findValue(angle);
        degreesProperty.setValue(angle);

        ReInitTextColor();

    }

    public void UpdateArrowToMaxValue()
    {
        double angle = findMax(((PolarPlotSeries)(this.getXYPane().getListOfSeries().get(0))).getXyItems1());
        UpdateArrow(angle);
    }


    private double findValue(double angle)
    {
        List<XYChartItem> xyChartItem = ((PolarPlotSeries)(this.getXYPane().getListOfSeries().get(0))).getXyItems1();
        double n = 360./xyChartItem.size();
        double value = 0;

        for(int i = 0; i < xyChartItem.size() - 1; i++)
        {
            if(xyChartItem.get(i).getX() <= angle && xyChartItem.get(i+1).getX() >= angle)
            {
                value = (angle/n - i) * (xyChartItem.get(i+1).getY() - xyChartItem.get(i).getY()) + xyChartItem.get(i).getY();
                break;
            }

        }

        valueProperty.setValue(value);

        return value;
    }

    private double findMax(List<XYChartItem> xyItems1)
    {
        double max=0.;
        int maxIndex = 0;
        for(int i = 0; i < xyItems1.size(); i++)
        {
            if(max <= xyItems1.get(i).getY())
            {
                max = xyItems1.get(i).getY();
                maxIndex = i;
            }
        }

        degreesProperty.setValue((xyItems1.get(maxIndex).getX()));
        valueProperty.setValue(max);

        return maxIndex*(360/xyItems1.size());
    }


    private void ReInitTextColor()
    {
        double width  = getXYPane().getWidth() - getXYPane().getInsets().getLeft() - getXYPane().getInsets().getRight();
        double height = getXYPane().getHeight() - getXYPane().getInsets().getTop() - getXYPane().getInsets().getBottom();
        double size   = width < height ? width : height;

        Font   font         = Fonts.latoRegular(0.035 * size);
        Helper.drawTextWithBackground(((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D(),
                "0",font,Color.rgb(49,49,49),Color.WHITE,0.5*size, 0.5*size - size * 0.018);
        Helper.drawTextWithBackground(((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D(),
                "255",font,Color.rgb(49,49,49),Color.WHITE,0.5*size, 0.5*size - 0.90 * size * 0.48);


        ((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D().setFont(Fonts.latoRegular(0.04 * size));
        for (int i = 0 ; i < 360.0 / this.getXYPane().getPolarTickStep().get(); i++) {
            ((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D().fillText(String.format(Locale.US, "%.0f", i * this.getXYPane().getPolarTickStep().get()), 0.5*size, size * 0.02);
            Helper.rotateCtx(((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D(), 0.5*size, 0.5*size, this.getXYPane().getPolarTickStep().get());
            ((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D().setFill(Color.rgb(0,198,255));
        }
        //((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D().fillText(String.format(Locale.US, "0"), 0.5*size, size * 0.02);
        //((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D().setFill(Color.rgb(0,198,255));

        ((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D().restore();
        ((Canvas)this.getXYPane().getChildren().get(0)).getGraphicsContext2D().save();
    }

}
