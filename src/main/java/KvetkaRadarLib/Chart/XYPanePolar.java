package KvetkaRadarLib.Chart;

import KvetkaRadarLib.Chart.PolarPlotSeries;
import eu.hansolo.fx.charts.*;
import eu.hansolo.fx.charts.data.XYItem;

public class XYPanePolar<XYChartItem extends XYItem> extends XYPane<XYChartItem>{


   public XYPanePolar(PolarPlotSeries series)
    {
        super(series);
        if(series.getXyItems1().size() > 0) {
            this.setLowerBoundY(60);
            this.setUpperBoundY(255);
        }
    }

}
