package KvetkaRadarLib.Chart;

import eu.hansolo.fx.charts.data.XYChartItem;
import javafx.beans.InvalidationListener;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.transform.Rotate;

import java.awt.event.ActionEvent;
import java.util.List;

public class ArrowCanvas extends Canvas {

    private GraphicsContext gc;
    private double angle;

    public double getAngle()
    {
        return angle;
    }

    public ArrowCanvas() {
        draw(0);
    }

    public void UpdateArrow(double angle)
    {
        this.angle = angle;
        draw(angle);
    }


    private void draw(double angle) {

        double height = getHeight();
        double width = getWidth();


        this.getGraphicsContext2D().clearRect(-1000,-1000,width +1000,height+1000);
        gc  = getGraphicsContext2D();



        double radius = Math.min(height,width);
        double x = Math.abs((radius/2.25) * Math.sin(Math.toRadians(angle)));
        double y = Math.abs((radius/2.25) * Math.cos(Math.toRadians(angle)));
        if(angle >= 0 && angle <= 90)
        {
            y=-y +(height/2);
            x += (width/2);
        }
        if(angle > 90 && angle <= 180)
        {
            x += (width/2);
            y += (height/2);
        }
        if(angle > 180 && angle <= 270)
        {
            x=-x + (width/2);
            y+=(height/2);
        }

        if(angle > 270. && angle < 360.)
        {
            y=-y+(height/2);
            x=-x+(width/2);
        }




        gc.beginPath();
        gc.setStroke(Color.rgb(220,220,220));
        gc.setLineWidth(1.85);
        gc.strokeLine(x, y,width/2, height/2 );

        double angleBetweenArrowsParts = Math.toRadians(35.0);
        double arrowLength = 10.0;
        double dx = width/2- x;
        double dy = height/2 - y;
        double angleArrowsParts = Math.atan2(dy, dx);
        double x1 = Math.cos(angleArrowsParts + angleBetweenArrowsParts) * arrowLength + x;
        double y1 = Math.sin(angleArrowsParts + angleBetweenArrowsParts) * arrowLength + y;

        double x2 = Math.cos(angleArrowsParts - angleBetweenArrowsParts) * arrowLength + x;
        double y2 = Math.sin(angleArrowsParts - angleBetweenArrowsParts) * arrowLength + y;
        gc.strokeLine(x, y, x1, y1);
        gc.strokeLine(x, y, x2, y2);
        gc.restore();
        gc.save();
        gc.closePath();
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }

    @Override
    public double prefHeight(double width) {
        return getHeight();
    }

}
