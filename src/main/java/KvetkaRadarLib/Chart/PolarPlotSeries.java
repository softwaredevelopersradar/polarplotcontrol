package KvetkaRadarLib.Chart;

import eu.hansolo.fx.charts.*;
import eu.hansolo.fx.charts.data.XYChartItem;
import eu.hansolo.fx.charts.series.XYSeries;
import eu.hansolo.fx.charts.tools.Order;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

import static eu.hansolo.fx.charts.tools.Helper.orderXYChartItemsByX;

public class PolarPlotSeries extends XYSeries {

   private List<XYChartItem> xyItems1;

    public PolarPlotSeries()
    {
        xyItems1 = new ArrayList<>();
        List<Double> data = new ArrayList<Double>();
        data.add(0.);
        setData(data);
        initialization();
    }


    public <T> void setData(List<T> data)
    {
        xyItems1 = new ArrayList<>();
        double degree = 0.;
        double n = 360./data.size() ;
        for(int i = 0; i < data.size(); i++ )
        {
            degree = n*(double)i;
            xyItems1.add(new XYChartItem(degree, (double)data.get(i)));
        }


        this.setItems(xyItems1);
        this.refresh();
    }


    public void initialization() {
        this.setTextFill(Color.RED);
        orderXYChartItemsByX((List<eu.hansolo.fx.charts.data.XYChartItem>) xyItems1, Order.ASCENDING);

        this.setItems(xyItems1);
        this.setChartType(ChartType.SMOOTH_POLAR);
        this.setFill(Color.WHITE);
        this.setStroke(Color.rgb(188, 193, 80));
        this.setSymbolStroke(Color.rgb(151,52,0));
        this.setSymbolFill(Color.rgb(151,146,0));
        this.setSymbol(Symbol.CIRCLE);
        this.setFill(Color.rgb(211,222,0,0.5));
        this.setSymbolSize(4);
        this.setName("Name");

    }

    public List<XYChartItem> getXyItems1()
    {
        return xyItems1;
    }
}
