package KvetkaRadarLib;


import KvetkaRadarLib.Interface.IArrowCallBack;
import KvetkaRadarLib.Interface.ILimbCallBack;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.List;

public class ChartPolarWithArrow extends AnchorPane {


    @FXML
    private OnlyChart_PolarWithArrow chart;

    @FXML
    private Label azimuth;

    @FXML
    private Label value;

    private IArrowCallBack iAzimuthCallBack;
    private ILimbCallBack iLimbCallBack;
    private double currentAzimuth = 0;
    private double currentValue = 0;


    public ChartPolarWithArrow() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/RadarControl.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        chart.getDegreesProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                azimuth.setText("α = ".concat(String.valueOf(Math.round(observableValue.getValue().doubleValue()*10.)/10.).concat("° ")));
                if (currentAzimuth != observableValue.getValue().doubleValue())
                {
                    currentAzimuth = observableValue.getValue().doubleValue();
                    //iAzimuthCallBack.changedAzimuth(currentAzimuth, currentValue);
                }
            }
        });

        chart.getValueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                value.setText("U = ".concat(String.valueOf(Math.round(observableValue.getValue().doubleValue() * 10.)/10.).concat(" Дб ")));

                if(currentValue != observableValue.getValue().doubleValue())
                {
                    currentValue = observableValue.getValue().doubleValue();
                    //iLimbCallBack.changeValue(currentAzimuth, currentValue);
                }
            }
        });
    }

    /**
     * registers a callback to access events
     *
     * @param iAzimuthCallBack instance of class which implements <code>IAzimuthCallBack<code/> interface
     */
    public void registerCallBackArrowEvents(IArrowCallBack iAzimuthCallBack)
    {
        this.iAzimuthCallBack = iAzimuthCallBack;
    }

    /**
     * registers a callback to access events
     *
     * @param iLimbCallBack instance of class which implements <code>ILimbCallBack<code/> interface
     */
    public void registerCallBackLimbEvents(ILimbCallBack iLimbCallBack)
    {
        this.iLimbCallBack = iLimbCallBack;
    }


    public <T> void UpdateChart(List<T> Data)
    {
        chart.UpdateChart(Data);
    }

    public void UpdateArrow(double angle)
    {
        chart.UpdateArrow(angle);
    }

    public void UpdateArrowToMaxValue()
    {
        chart.UpdateArrowToMaxValue();
    }
}
