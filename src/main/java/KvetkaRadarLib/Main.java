package KvetkaRadarLib;

import KvetkaRadarLib.Interface.IArrowCallBack;
import KvetkaRadarLib.Interface.ILimbCallBack;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

public class Main extends Application {
    List<Double> data;
    @Override
    public void start(Stage primaryStage) throws Exception{

        int size = 5;
        ChartPolarWithArrow paneSpectr = new ChartPolarWithArrow();
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(paneSpectr, 400, 400));
        primaryStage.show();
        //paneSpectr.registerCallBackSignalEvents(this);
        paneSpectr.UpdateArrow(ThreadLocalRandom.current().nextDouble(0, 360));


        final TimerTask task = new TimerTask() {
            long startTime = System.currentTimeMillis();

            @Override
            public void run() {
                data = new ArrayList<>();
                for(int i = 0; i < size; i++)
                {
                    data.add(i, 1.0);//(double) ThreadLocalRandom.current().nextInt(100, 400)/10.0);

                }
                data.add(1, ((double)ThreadLocalRandom.current().nextInt(2300, 2500))/10.0);
                data.add(2, ((double)ThreadLocalRandom.current().nextInt(1000, 1100))/10.0);
                data.add(3, ((double)ThreadLocalRandom.current().nextInt(1500, 1600))/10.0);
                data.add(4, ((double)ThreadLocalRandom.current().nextInt(1800, 1900))/10.0);
                Platform.runLater(() -> {
                    //paneSpectr.UpdateChart(data);

                });
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task,500,5000);
    }


    public static void main(String[] args) {
        launch(args);
    }

}
