package KvetkaRadarLib.Interface;

public interface IArrowCallBack {

     void changedAzimuth(double azimuth, double value);
}
