package KvetkaRadarLib.Interface;

public interface ILimbCallBack {

    void changeValue(double azimuth, double value);
}
